import {showModal} from '../modal/modal';
import {createFighterImage} from '../fighterPreview';


export function showWinnerModal(fighter) {
  // call showModal function 
  const title = `Winner is ${fighter.name}`;
  const bodyElement = createFighterImage(fighter);

  showModal({ title, bodyElement,})

}
