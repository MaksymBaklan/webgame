import { controls } from '../../constants/controls';
import { changeHealthBar } from '../components/fightDetails';

var pressedKeys = {};
var pressedKeysForCriticalHit = {};
const [q, w, ee] = controls.PlayerOneCriticalHitCombination;
const [u, i, o] = controls.PlayerTwoCriticalHitCombination;

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    
    onkeydown = function (e) {
      if (pressedKeys[e.code] && pressedKeysForCriticalHit[e.code]) return;
      pressedKeys[e.code] = true;
      pressedKeysForCriticalHit[e.code] = true;    
    }
    
    onkeyup = function (e) {
      delete pressedKeys[e.code];
      delete pressedKeysForCriticalHit[e.code];
    }
    
    let canLeftDoCriticalHit = true;
    let canRightDoCriticalHit = true;
    onkeypress = function (e) {
      
      if (e.code === controls.PlayerOneAttack && !(controls.PlayerOneBlock in pressedKeys) ) {
        const damage = getDamage(firstFighter, secondFighter);
        const damageInPercent = damage * 100 / secondFighter.health;
        
        if ( changeHealthBar(damageInPercent, 'right') ) {
          resolve(firstFighter);
        } 
    
      } else if (e.code === controls.PlayerTwoAttack && !(controls.PlayerTwoBlock in pressedKeys) ) {
        const damage = getDamage(secondFighter, firstFighter);
        const damageInPercent = damage * 100 / firstFighter.health;
        
        if ( changeHealthBar(damageInPercent, 'left') ) {
          resolve(secondFighter);
        }
      } else if ( (e.code == q && w in pressedKeysForCriticalHit && ee in pressedKeysForCriticalHit && canLeftDoCriticalHit) ||
                  (e.code == w && ee in pressedKeysForCriticalHit && q in pressedKeysForCriticalHit && canLeftDoCriticalHit) ||
                  (e.code == ee && q in pressedKeysForCriticalHit && w in pressedKeysForCriticalHit && canLeftDoCriticalHit) ){
        
        const damage = firstFighter.attack * 2;
        const damageInPercent = damage * 100 / secondFighter.health;

        canLeftDoCriticalHit = false;
        setTimeout(() => canLeftDoCriticalHit = true, 10000);

        if ( changeHealthBar(damageInPercent, 'right') ) {
          resolve(firstFighter);
        }
      } else if ( (e.code == o && i in pressedKeysForCriticalHit && u in pressedKeysForCriticalHit && canRightDoCriticalHit) ||
      (e.code == u && i in pressedKeysForCriticalHit && o in pressedKeysForCriticalHit && canRightDoCriticalHit) ||
      (e.code == i && u in pressedKeysForCriticalHit && o in pressedKeysForCriticalHit && canRightDoCriticalHit) ){

        const damage = firstFighter.attack * 2;
        const damageInPercent = damage * 100 / secondFighter.health;

        canRightDoCriticalHit = false;
        setTimeout(() => canRightDoCriticalHit = true, 10000);

        if ( changeHealthBar(damageInPercent, 'left') ) {
          resolve(secondFighter);
        }
      }

    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  if (controls.PlayerTwoBlock in pressedKeys || controls.PlayerOneBlock in pressedKeys) {
    const result = getHitPower(attacker) - getBlockPower(defender);
    return result > 0 ? result : 0;
  }else {
    return getHitPower(attacker);
  }
}

export function getHitPower(fighter) {
  // return hit power
  const {attack} = fighter;
  return attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  const {defense} = fighter;
  return defense * (Math.random() + 1);
}
