let fullDamageRight = 0;
let fullDamageLeft = 0;


export function changeHealthBar(damage, fighter) {
    if (fighter === 'right') {
        fullDamageRight = fullDamageRight + damage > 100 ? 100 : fullDamageRight + damage;
        
        const bar = document.getElementById('right-fighter-indicator');
        const barSize = 100 - fullDamageRight + '%';
    
        bar.style.width = barSize;

    } else if (fighter === 'left') {
        fullDamageLeft = fullDamageLeft + damage > 100 ? 100 : fullDamageLeft + damage;

        const bar = document.getElementById('left-fighter-indicator');
        const barSize = 100 - fullDamageLeft + '%';
    
        bar.style.width = barSize;
    }
    
    if (fullDamageLeft === 100 || fullDamageRight === 100) return true;
}
